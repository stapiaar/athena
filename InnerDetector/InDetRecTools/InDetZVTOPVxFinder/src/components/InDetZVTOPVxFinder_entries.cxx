#include "InDetZVTOPVxFinder/ZVTOP_Tool.h"
#include "InDetZVTOPVxFinder/ZVTOP_SecVtxTool.h"
#include "InDetZVTOPVxFinder/ZVTOP_SpatialPointFinder.h"
#include "InDetZVTOPVxFinder/ZVTOP_SlowSpatialPointFinder.h"
#include "InDetZVTOPVxFinder/ZVTOP_TrkProbTubeCalc.h"
#include "InDetZVTOPVxFinder/ZVTOP_SimpleVtxProbCalc.h"
#include "InDetZVTOPVxFinder/ZVTOP_AmbiguitySolver.h"

DECLARE_COMPONENT( InDet::ZVTOP_Tool )
DECLARE_COMPONENT( InDet::ZVTOP_SecVtxTool )
DECLARE_COMPONENT( InDet::ZVTOP_SpatialPointFinder )
DECLARE_COMPONENT( InDet::ZVTOP_SlowSpatialPointFinder )
DECLARE_COMPONENT( InDet::ZVTOP_TrkProbTubeCalc )
DECLARE_COMPONENT( InDet::ZVTOP_SimpleVtxProbCalc )
DECLARE_COMPONENT( InDet::ZVTOP_AmbiguitySolver )

